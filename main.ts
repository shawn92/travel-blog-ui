import "./polyfills";

import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./src/module";

platformBrowserDynamic().bootstrapModule(AppModule);