import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { BlogModule, BLOG_REDUCER, AuthGuard } from "@sightseeker/blog.sdk";

import { storeLogger } from "ngrx-store-logger";

import "rxjs/operator/do";
import "rxjs/operator/filter";

import { BLOG_ROUTES } from "./routes";

import { AppComponent } from "./app.component";
import { AdminPage } from "./admin/admin.page";
import { LoginPage } from "./login/login.page";

export function logger(reducer: any): any {
    return storeLogger()(reducer);
}

export const metaReducers = [logger];

export const PAGES = [
    AdminPage,
    LoginPage
];

@NgModule({ 
    declarations: [
        AppComponent,
        ...PAGES
    ],
    imports: [
        BlogModule,
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(BLOG_ROUTES),
        StoreModule.forRoot({
            ...BLOG_REDUCER
        }, { metaReducers })
    ],
    providers: [
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }