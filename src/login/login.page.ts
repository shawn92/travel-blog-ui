import { Component, ChangeDetectionStrategy, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { BlogState, AuthActions, AuthSelector, AuthState } from "@sightseeker/blog.sdk";

import { merge } from "rxjs/observable/merge";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: "ss-login",
    templateUrl: "./login.page.html",
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginPage implements OnInit, OnDestroy {
    username: string;
    password: string;
    
    private returnUrl: string;
    private data$$: Subscription;
    
    constructor(
        private authAction: AuthActions,
        private authSelector: AuthSelector,
        private router: Router,
        private route: ActivatedRoute,
        private store: Store<BlogState> 
    ) {}
    
    ngOnInit() {
        const returnUrl$ = this.route.queryParams
            .do(params => this.returnUrl = params["returnUrl"] || "/");
        
        const authenticated$ = this.store.select(this.authSelector.getToken())
            .filter(token => !!token)
            .do(token => this.router.navigateByUrl(this.returnUrl));
        
        this.data$$ = merge(returnUrl$, authenticated$).subscribe();
    }
    
    onLogin() {
        this.store.dispatch(this.authAction.login(this.username, this.password));
    }
    
    ngOnDestroy(): void {
        this.data$$.unsubscribe();
    }
}