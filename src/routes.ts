import { Routes } from "@angular/router";
import { AuthGuard } from "@sightseeker/blog.sdk";

import { AdminPage } from "./admin/admin.page";
import { LoginPage } from "./login/login.page";

export const BLOG_ROUTES: Routes = [
    {
        path: "admin",
        component: AdminPage,
        canActivate: [AuthGuard]
    },
    {
        path: "login",
        component: LoginPage
    }
    // {
    //     path: "**",
    //     component: NotFoundPage
    // }
];