const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: {
        app: "./main.ts",
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "travel-blog.ui.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ["ts-loader", "angular2-template-loader"],
                exclude: "/node_modules/"
            },
            {
                test: /\.html$/,
                use: "html-loader",
                exclude: "/node_modules/"
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: true
            },
            template: "./public/index.html"
        })
    ],
    resolve: {
        extensions: [".ts", ".js"]
    },
    devServer: {
        contentBase: path.resolve(__dirname, "dist"),
        port: 3000
    }
}